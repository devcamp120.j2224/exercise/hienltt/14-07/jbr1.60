import com.devcamp.Date;

public class App {
    public static void main(String[] args) throws Exception {
        
        Date date1 = new Date(3, 8, 1993);
        Date date2 = new Date(13, 11, 1995);

        System.out.println("date1: " + date1);
        System.out.println("date2: " + date2);
    }
}
